﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using intro.Models;
using intro.Services;
using Microsoft.EntityFrameworkCore;

namespace intro.Controllers
{
    public class StudentsController : Controller
    {
        private StudentContext students;

        public StudentContext Students
        {
            get => students;
            set => students = value;
        }

        public StudentsController(StudentContext students)
        {
            this.students = students;
        }
        // GET
        public IActionResult Index()
        {
            return View(students.Students.ToArray());
        }

        public IActionResult tableView()
        {
            return View(students.Students.ToArray());
        }

        public IActionResult cardView()
        {
            return View(students.Students.ToArray());
        }
    }
}