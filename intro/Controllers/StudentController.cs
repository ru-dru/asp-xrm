﻿using System;
using System.Collections.Generic;
using System.Linq;
using intro.Models;
using intro.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.DependencyInjection;

namespace intro.Controllers
{
    public class StudentController : Controller
    {

        private StudentContext students;

        public StudentController(StudentContext students)
        {
            this.students = students;
        }
        
        [HttpPost]
        public ActionResult add(
            string firstName,
            string secondName,
            int course,
            int y,
            int m,
            int d
            )
        {
            if (ModelState.IsValid)
            {
                students.Students.Add(
                    new Student
                    {
                        FirstName = firstName,
                        SecondName = secondName,
                        BirthDay = new DateTime(y, m, d),
                        CourseNumber = course,
                        Id = 0
                    }
                );
                students.SaveChanges();

                return Json(students.Students.ToDictionary(x=>x.Id));
            }

            return BadRequest();
        }

        [HttpGet]
        public IActionResult get(int id)
        {
            // получение сервиса через HttpContext
            var service = HttpContext.RequestServices.GetService<IContainer<Student>>();
            try
            {
                var student = students.Students.Where(x=>x.Id==id).ToArray()[0];
                return View(student);
            }
            catch (KeyNotFoundException e)
            {
                return BadRequest();
            }
        }

        [HttpDelete]
        public IActionResult delete(int id)
        {
            try
            {
                var r = students.Students.Where(x=>x.Id == id).ToArray();
                if (r.Length == 0)
                    throw new Exception($"no student with id {id}");

                students.Students.Remove(r[0]);
                students.SaveChanges();

                return Json(students.Students.ToDictionary(x => x.Id));
            }
            catch (Exception e)
            {
                Console.Error.Write(e.Message);
                return BadRequest();
            }
        }

        [HttpPut]
        public ActionResult change(
            int id,
            string firstName,
            string secondName,
            int course,
            int y,
            int m,
            int d
        )
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var r = students.Students.Where(x => x.Id == id).ToArray();
                    if (r.Length == 0)
                        throw new Exception($"no student with id {id}");

                    var newValue = r[0];
                    
                    newValue.BirthDay = new DateTime(y, m, d);
                    newValue.CourseNumber = course;
                    newValue.FirstName = firstName;
                    newValue.SecondName = secondName;

                    students.Students.Update(newValue);
                    students.SaveChanges();

                    return Json(students.Students.ToDictionary(x => x.Id));
                }
                catch (Exception e)
                {
                    Console.Error.WriteLine(e.Message);
                    return BadRequest();
                }
            }

            return BadRequest();
        }

        [HttpGet]
        public IActionResult edit([FromServices]IContainer<Student> studentsContainer, int id)
        {
            try
            {
                return View(students.Students.Where(x=>x.Id==id).ToArray()[0]);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return BadRequest();
            }
        }
        
        [HttpPost]
        public IActionResult edit(Student student)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    //Подменяем студентов. Не работает...
                    /*var instance = ActivatorUtilities
                        .CreateInstance<IContainer<Student>>(HttpContext.RequestServices, new StudentsCollection());
                    */
                    students.Update(student);
                    students.SaveChanges();
                    return Redirect("~/students");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return View(student);
                }
            }
            return View(student);
        }
    }
}