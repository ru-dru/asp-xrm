﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace intro.Models
{
    public class Student:IValidatableObject
    {
        [Required]
        [Range(1, int.MaxValue)]
        [DisplayName("id")]
        public int Id { get; set; }
        
        [MinLength(2)]
        [MaxLength(20)]
        [Required]
        [DisplayName("Имя")]
        public string FirstName { get; set; }
        
        [MinLength(2)]
        [MaxLength(20)]
        [Required]
        [DisplayName("Фамилия")]
        public string SecondName { get; set; }
        
        [Required]
        [DisplayName("Дата рождения")]
        public DateTime BirthDay { get; set; }
        
        [Range(1,6)]    //Бакалавры и магистранты
        [DisplayName("Курс")]
        public int CourseNumber { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (BirthDay > new DateTime(2000,01, 01) ||
                BirthDay < new DateTime(1900, 01, 01))
                yield return new ValidationResult("Надо дату заполнить");
        }
    }
}