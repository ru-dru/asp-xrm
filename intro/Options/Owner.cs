﻿namespace intro.Options
{
    public class Owner
    {
        private string
            firstName,
            secondName,
            email;

        public Owner(string firstName, string secondName, string email)
        {
            this.firstName = firstName;
            this.secondName = secondName;
            this.email = email;
        }

        public string FirstName
        {
            get => firstName;
            set => firstName = value;
        }

        public string SecondName
        {
            get => secondName;
            set => secondName = value;
        }

        public string Email
        {
            get => email;
            set => email = value;
        }
    }
}