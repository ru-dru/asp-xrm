﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using intro.Models;
using intro.Options;
using intro.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;

namespace intro
{
    public class Startup
    {
        
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Configuration.GetConnectionString("DefaultConnection");

            services.AddDbContext<StudentContext>(
                options => options.UseMySql(connectionString));

            services.AddMvc();
            services.AddSingleton<IContainer<Student>, StudentsContainer>();
            services.AddTransient<IStorage<int, Student>, StudentsCollection>();
            services.Configure<Owner>(Configuration.GetSection("owner"));
        }

        public class TimeMeasuringMiddleWare
        {
            private readonly RequestDelegate _next;

            public TimeMeasuringMiddleWare(RequestDelegate next)
            {
                _next = next;
            }

            public async Task Invoke(HttpContext context)
            {
                var start = DateTime.Now;
                await _next.Invoke(context);
                var end = DateTime.Now;
                context.Response.WriteAsync((end - start).ToString());
            }
        }
        
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            /*app.Use(async (context, next) =>
            {
                var start = DateTime.Now;
                await next();
                var end = DateTime.Now;
                context.Response.WriteAsync((end - start).ToString());
            });*/
            //app.UseMiddleware<TimeMeasuringMiddleWare>();
            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
                
                routes.MapRoute(
                    name: "student",
                    template: "student/*/",
                    defaults: new { controller = "Student", action = "Index" });

                routes.MapRoute(
                    name: "students",
                    template: "students/*/",
                    defaults: new {controller = "Students", action = "Index"}
                );
            });
        }
    }
}
