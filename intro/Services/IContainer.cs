﻿namespace intro.Services
{
    public interface IContainer<T>
    {
        int maxId();
        void create(object newObject);
        T read(int id);
        T update(int id, T newValue);
        void delete(int id);
        IStorage<int, T> GetStorage();
    }
}