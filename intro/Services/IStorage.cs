﻿using System.Collections;
using System.Collections.Generic;

namespace intro.Services
{
    public interface IStorage<key_T, value_T>
    {
        ICollection<KeyValuePair<key_T, value_T>> GetCollection();
        HashSet<key_T> keys();
        value_T get(key_T id);

    }
}