﻿using System;
using System.Collections.Generic;
using System.Linq;
using intro.Models;

namespace intro.Services
{
    public class StudentsContainer:IContainer<Student>
    {
        private IStorage<int, Student> _storage;

        public int maxId()
        {
            if (_storage.GetCollection().Count > 0)
                return _storage.keys().Max();
            return 0;
        }
        
        //получение сервиса как параметр конструктора
        public StudentsContainer(IStorage<int, Student> storage)
        {
            _storage = storage;
        }

        public void create(object newObject)
        {
            _storage.GetCollection()
                .Add(new KeyValuePair<int, Student>(maxId()+1, (Student)newObject));
        }

        public Student read(int id)
        {
            return _storage.get(id);
        }

        public Student update(int id, Student newValue)
        {
            var old = _storage.get(id);
            _storage.GetCollection().Remove(new KeyValuePair<int, Student>(id, old));
            old = newValue;
            _storage.GetCollection().Add(new KeyValuePair<int, Student>(id, old));

            return _storage.get(id);
        }

        public void delete(int id)
        {
            var target = _storage.get(id);
            _storage.GetCollection().Remove(new KeyValuePair<int, Student>(id, target));
            
        }

        public IStorage<int, Student> GetStorage()
        {
            return _storage;
        }
    }
}