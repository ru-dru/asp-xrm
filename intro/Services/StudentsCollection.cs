﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using intro.Models;

namespace intro.Services
{
    public class StudentsCollection:IStorage<int, Student>
    {
        private Dictionary<int, Student> _students;

        public StudentsCollection()
        {
            _students =  new Dictionary<int, Student>();
        }

        public ICollection<KeyValuePair<int, Student>> GetCollection()
        {
            return _students;
        }

        public HashSet<int> keys()
        {
            var r =_students.Keys;
            return r.ToHashSet();
        }

        public Student get(int id)
        {
            if (!_students.ContainsKey(id))
                throw new KeyNotFoundException($"Student with id {id} not found");

            return _students[id];
        }
    }
}